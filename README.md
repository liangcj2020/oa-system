# OA-System

#### 介绍
JavaWeb实现一个简易的OA管理系统。该仓库两条分支，主分支是JSP回传。另一条是利用Ajax实现异步操作。

两种身份登陆：管理员可以对模块进行增删改，员工只能查（文件模块可以上传）。
#### 参考图
![img.png](pic/img.png)

![img_1.png](pic/img_1.png)

