package com.oa.controller;


import com.oa.bean.Dept;
import com.oa.bean.Job;
import com.oa.bean.PageInfo;
import com.oa.dao.DeptDao;
import com.oa.dao.JobDao;
import com.oa.service.DeptService;
import com.oa.service.JobService;
import com.oa.service.impl.DeptServiceImpl;
import com.oa.service.impl.JobServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

//使用JobController来接收和处理job模块的所有请求操作
//目的：一个请求处理类映射多个uri
@WebServlet(urlPatterns = { "/deleteJob", "/queryJob", "/insertJob", "/toUpdateJob", "/updateJob"})
@MultipartConfig //支持文件上传的注解
public class JobController extends HttpServlet {

    //获取业务层接口的对象
    JobService jobService = new JobServiceImpl();

    //doGet方法是用于接收get请求的
    @Override   //          request  请求对象           response  响应对象
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");//请求对象设置编码
        //获取请求uri
        String uri = request.getRequestURI();
        // response.getWriter().append("JobController--"+uri);
        if (uri.contains("queryJob")) {
            queryJob(request, response);     //查询请求处理方法
        } else if (uri.contains("insertJob")) {
            insertJob(request, response);   //添加请求处理方法
        } else if (uri.contains("toUpdateJob")) {
            toUpdateJob(request, response); //修改页面请求
        } else if (uri.contains("updateJob")) {
            updateJob(request, response); //修改请求
        } else if (uri.contains("deleteJob")) {
            deleteJob(request, response); //删除请求
        }

    }

    //删除请求处理方法
    protected void deleteJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1、从请求对象中获取请求参数 通过getParameter方法根据key来获取value
        String str_id = request.getParameter("id");
        int id = Integer.parseInt(str_id);//转换为int类型
        //2、调用业务层方法实现功能
        jobService.deleteJob(id);
        //3、作出响应--重新查询
        request.getRequestDispatcher("queryJob").forward(request, response);
    }

    //修改请求处理方法
    protected void updateJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1、从请求对象中获取请求参数 通过getParameter方法根据key来获取value
        String str_id = request.getParameter("id");
        int id = Integer.parseInt(str_id);//转换为int类型
        String name = request.getParameter("name");//没有获取到该数据则为null
        String remark = request.getParameter("remark");

        //创建job对象将数据进行封装
        Job job = new Job();
        job.setId(id);
        job.setName(name);
        job.setRemark(remark);


        //2、调用业务层方法实现功能
        boolean isok = jobService.updateJob(job);
        //3、作出响应--跳转页面
        if (isok) {//成功则重新查询数据
            request.getRequestDispatcher("queryJob").forward(request, response);
        } else {//失败跳转回到修改页面
            request.getRequestDispatcher("toUpdateJob?id=" + id).forward(request, response);
        }
    }

    //跳转查询请求处理方法
    protected void toUpdateJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1、获取id，根据id查询job
        String jobid = request.getParameter("id");
        int id = Integer.parseInt(jobid);
        Job job = jobService.queryJobById(id);
        //2、保存job对象数据保存在属性作用
        request.setAttribute("job", job);
        //3、跳转JobUpdate.jsp页面
        request.getRequestDispatcher("JobUpdate.jsp").forward(request, response);
    }

    //查询请求处理方法
    protected void queryJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取跳转页面
        String pn = request.getParameter("pn");
        int pageNo = 1;
        if(pn!=null){
            pageNo = Integer.parseInt(pn);
        }
        //获取查询的name数据search
        String search = request.getParameter("search")==null?"":request.getParameter("search");
        //创建分页的对象
        PageInfo page = new PageInfo(pageNo, JobDao.queryJobCount(search));
        //1、调用业务层方法查询数据
        List<Job> jobList = jobService.queryJob(page,search);
        //2、保存数据到属性作用域中
        request.setAttribute("jobList", jobList);
        request.setAttribute("pageInfo", page);
        request.setAttribute("username", search);
        //3、跳转到JobIndex.jsp页面进行数据显示
        request.getRequestDispatcher("JobIndex.jsp").forward(request, response);
    }

    //添加请求处理方法
    protected void insertJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1、从请求对象中获取请求参数 通过getParameter方法根据key来获取value
        String name = request.getParameter("name");//没有获取到该数据则为null
        String remark = request.getParameter("remark");

        //创建job对象将数据进行封装
        Job job = new Job();
        job.setName(name);
        job.setRemark(remark);


        //2、调用业务层方法实现功能
        boolean isok =  jobService.insertJob(job);
        //3、作出响应--跳转页面
        if (isok) {//成功则跳转到首页
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {//失败跳转回到登录页面
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
    }

    //doPost方法是用于接收Post请求的
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //调用了上面的doGet，目的是方便操作【不管是get、post都可以在doGet中编写处理代码】
        doGet(request, response);
    }
}
