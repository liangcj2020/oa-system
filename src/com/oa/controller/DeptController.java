package com.oa.controller;


import com.oa.bean.Dept;
import com.oa.bean.PageInfo;
import com.oa.bean.User;
import com.oa.dao.DeptDao;
import com.oa.dao.UserDao;
import com.oa.service.DeptService;
import com.oa.service.UserService;
import com.oa.service.impl.DeptServiceImpl;
import com.oa.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.List;

//使用DeptController来接收和处理dept模块的所有请求操作
//目的：一个请求处理类映射多个uri
@WebServlet(urlPatterns = { "/deleteDept", "/queryDept", "/insertDept", "/toUpdateDept", "/updateDept"})
@MultipartConfig //支持文件上传的注解
public class DeptController extends HttpServlet {

    //获取业务层接口的对象
    DeptService deptService = new DeptServiceImpl();

    //doGet方法是用于接收get请求的
    @Override   //          request  请求对象           response  响应对象
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");//请求对象设置编码
        //获取请求uri
        String uri = request.getRequestURI();
        // response.getWriter().append("DeptController--"+uri);
        if (uri.contains("queryDept")) {
            queryDept(request, response);     //查询请求处理方法
        } else if (uri.contains("insertDept")) {
            insertDept(request, response);   //添加请求处理方法
        } else if (uri.contains("toUpdateDept")) {
            toUpdateDept(request, response); //修改页面请求
        } else if (uri.contains("updateDept")) {
            updateDept(request, response); //修改请求
        } else if (uri.contains("deleteDept")) {
            deleteDept(request, response); //删除请求
        }

    }

    //删除请求处理方法
    protected void deleteDept(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1、从请求对象中获取请求参数 通过getParameter方法根据key来获取value
        String str_id = request.getParameter("id");
        int id = Integer.parseInt(str_id);//转换为int类型
        //2、调用业务层方法实现功能
        deptService.deleteDept(id);
        //3、作出响应--重新查询
        request.getRequestDispatcher("queryDept").forward(request, response);
    }

    //修改请求处理方法
    protected void updateDept(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1、从请求对象中获取请求参数 通过getParameter方法根据key来获取value
        String str_id = request.getParameter("id");
        int id = Integer.parseInt(str_id);//转换为int类型
        String name = request.getParameter("name");//没有获取到该数据则为null
        String remark = request.getParameter("remark");

        //创建dept对象将数据进行封装
        Dept dept = new Dept();
        dept.setId(id);
        dept.setName(name);
        dept.setRemark(remark);


        //2、调用业务层方法实现功能
        boolean isok = deptService.updateDept(dept);
        //3、作出响应--跳转页面
        if (isok) {//成功则重新查询数据
            request.getRequestDispatcher("queryDept").forward(request, response);
        } else {//失败跳转回到修改页面
            request.getRequestDispatcher("toUpdateDept?id=" + id).forward(request, response);
        }
    }

    //跳转查询请求处理方法
    protected void toUpdateDept(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1、获取id，根据id查询dept
        String deptid = request.getParameter("id");
        int id = Integer.parseInt(deptid);
        Dept dept = deptService.queryDeptById(id);
        //2、保存dept对象数据保存在属性作用
        request.setAttribute("dept", dept);
        //3、跳转DeptUpdate.jsp页面
        request.getRequestDispatcher("DeptUpdate.jsp").forward(request, response);
    }

    //查询请求处理方法
    protected void queryDept(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取跳转页面
        String pn = request.getParameter("pn");
        int pageNo = 1;
        if(pn!=null){
            pageNo = Integer.parseInt(pn);
        }
        //获取查询的name数据search
        String search = request.getParameter("search")==null?"":request.getParameter("search");
        //创建分页的对象
        PageInfo page = new PageInfo(pageNo, DeptDao.queryDeptCount(search));
        //1、调用业务层方法查询数据
        List<Dept> deptList = deptService.queryDept(page,search);
        //2、保存数据到属性作用域中
        request.setAttribute("deptList", deptList);
        request.setAttribute("pageInfo", page);
        request.setAttribute("username", search);
        //3、跳转到DeptIndex.jsp页面进行数据显示
        request.getRequestDispatcher("DeptIndex.jsp").forward(request, response);
    }

    //添加请求处理方法
    protected void insertDept(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1、从请求对象中获取请求参数 通过getParameter方法根据key来获取value
        String name = request.getParameter("name");//没有获取到该数据则为null
        String remark = request.getParameter("remark");

        //创建dept对象将数据进行封装
        Dept dept = new Dept();
        dept.setName(name);
        dept.setRemark(remark);


        //2、调用业务层方法实现功能
        boolean isok =  deptService.insertDept(dept);
        //3、作出响应--跳转页面
        if (isok) {//成功则跳转到首页
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {//失败跳转回到登录页面
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
    }

    //doPost方法是用于接收Post请求的
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //调用了上面的doGet，目的是方便操作【不管是get、post都可以在doGet中编写处理代码】
        doGet(request, response);
    }
}
